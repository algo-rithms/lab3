﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <chrono>
#include <iostream>

#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

double func1(double n) {
	return n;
}

double func2(double n) {
	return log(n);
}

double func3(double n) {
	return n * log(n);
}

double func4(double n) {
	return pow(n, 2);
}

double func5(double n) {
	return pow(2, n);
}

double func6(double n) {
	if (n == 0) {
		return 1;
	}
	else {
		return n * func6(n - 1);
	}
}

long long factorial(int a) {
	if (a == 0)
		return 1; // Базовий випадок: факторіал 0 дорівнює 1
	else
		return a * factorial(a - 1); // Рекурсивний випадок: факторіал a дорівнює a * факторіал (a-1)
}

// Функція порівняння для сортування у порядку спадання
int compare(const void* a, const void* b) {
	return (*(int*)b - *(int*)a);
}

// Функція для генерації масиву випадкових цифр вісімкової системи числення
void generate_random_octal_digits(int* array, int size) {
	for (int i = 0; i < size; i++) {
		array[i] = rand() % 8; // Випадкові цифри від 0 до 7
	}
}

// Функція для знаходження найбільшого можливого числа з масиву цифр
long long find_largest_number(int* array, int size) {
	// Сортування масиву у порядку спадання
	qsort(array, size, sizeof(int), compare);

	// Формування числа з відсортованих цифр
	long long largest_number = 0;
	for (int i = 0; i < size; i++) {
		largest_number = largest_number * 10 + array[i];
	}

	return largest_number;
}


int main() {


	auto begin = GETTIME();
	for (int i = 0; i <= 50; i++) {
		printf("[%d] [f(n) = n] %.2f\n", i, func1(i));
	}
	auto finish = GETTIME();
	auto elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n\n", elapsed_ns);



	begin = GETTIME();
	for (int i = 0; i <= 50; i++) {
		printf("[%d] [f(n) = log(n)] %.2f\n", i, func2(i));
	}
	finish = GETTIME();
	elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n\n", elapsed_ns);



	begin = GETTIME();
	for (int i = 0; i <= 50; i++) {
		printf("[%d] [f(n) = n * log(n)] %.2f\n", i, func3(i));
	}
	finish = GETTIME();
	elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n\n", elapsed_ns);
	


	begin = GETTIME();
	for (int i = 0; i <= 50; i++) {
		printf("[%d] [f(n) = n^2] %.2f\n", i, func4(i));
	}
	finish = GETTIME();
	elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n\n", elapsed_ns);



	begin = GETTIME();
	for (int i = 0; i <= 50; i++) {
		printf("[%d] [f(n) = 2^n] %.2f\n", i, func5(i));
	}
	finish = GETTIME();
	elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n\n", elapsed_ns);



	begin = GETTIME();
	for (int i = 0; i <= 50; i++) {
		printf("[%d] [f(n) = n!] %.2f\n", i, func6(i));
	}
	finish = GETTIME();
	elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n\n", elapsed_ns);
	


	for (int i = 0; i <= 20; i++) {
		begin = GETTIME();
		printf("Factorial number %d - %lld | ", i, factorial(i));
		finish = GETTIME();
		elapsed_ns = CALCTIME(finish - begin).count();
		printf("The time: %lld ms\n", elapsed_ns);
	}
	printf("\n");

	srand(time(NULL));
	for (int i = 0; i <= 50; i++) {
		begin = GETTIME();
		int array[10];
		generate_random_octal_digits(array, 10);
		printf("Generated array of octal numbers: ");
		for (int i = 0; i < 10; i++) {
			printf("%d ", array[i]);
		}
		long long largest_number = find_largest_number(array, 10);
		printf("| The largest possible number: %lld | ", largest_number);
		finish = GETTIME();
		elapsed_ns = CALCTIME(finish - begin).count();
		printf("The time: %lld ms\n", elapsed_ns);
	}
}